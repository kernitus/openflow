# OpenFlow

An very simple implementation of the OpenFlow protocol in Kotlin. Packets are described as an open class and extended to add more information in the _extras_ field. The information is serialised as a byte array and sent as a Datagram over the network using UDP, and deserialised at the other end.

## Packet layout

### Generic packet layout

`PacketType (Byte) - Frame ID (Byte) - Extras length? (Byte integer) - Extras? (Byte array)`

### EndToEnd packet layout

`PacketType - Frame ID - Extras length - Message (String) - Destination (String)`

### FlowTable packet layout

`PacketType - Frame ID - Extras length - Table (Array of Array)`

### PhysicalConnections packet layout

`PacketType - Frame ID - Extras length - is Endpoint (Boolean) - Connections (Array)`

## Algorithm

EndPoints sit at the edge of the network and are connected by Routers. Both endpoints and routers send information about their physical connections to the Controller using the PhysicalConnections packet, which in turn calculates the best routes through the network, and updates the Routers with FlowTables. The routes are calculated using Breadth-First-Search, as each hop is considered to have a cost of 1, rendering Dijkstra unnecessary. This is a simplified version of how the central routing algorithm of Open Shortest Path First works. The updated flow tables are sent to each router using the FlowTable packet type, and can be updated at any moment to reflect changes in the network topology. Finally, EndToEnd packets are sent by the endpoints and routed by the routers following each router's flowtable until the destination is reached. Each individual router is not aware of the overall topology of the network, but only which neighbour to forward packets that are coming from each specific neighbour.
