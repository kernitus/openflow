package openflow

import openflow.packets.FlowTablePacket
import openflow.packets.GenericPacket
import openflow.packets.PacketType
import openflow.packets.PhysicalConnectionsPacket
import java.net.InetAddress
import java.util.*
import kotlin.collections.HashMap

class Controller : Node() {

    /**
     * Used for representing shortest paths from one Endpoint to another to calculate flow tables
     */
    private data class EndToEndRoute(val startPoint: String, val endPoint: String, val route: List<String>)

    /**
     * Store data from the incoming physical connections packets
     * Key is router hostname and value is array of hostnames of connections
     */
    private val physicalConnections = HashMap<String, Array<String>>()

    /**
     * Store the entrypoints (the router into the network) for each endpoint
     * Key is hostname of endpoint and value is hostname of entry router
     */
    private val endpointConnections = HashMap<String, String>()

    /**
     * Stores the flowtables for each router
     */
    private var flowTables = HashMap<String, HashMap<String, String>>()

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val controller = Controller()
            println("Getting physical connections... Please enter 'done' when done:")
            controller.listen()
            while(readLine() != "done"){
                // block main thread until done
            }
            controller.sendFlowTables()
        }
    }

    override fun onReceipt(genericPacket: GenericPacket, sourceAddress: InetAddress) {
        when(genericPacket.type){
            PacketType.PHYSICAL_CONNECTIONS -> {
                val physicalConnectionsPacket = PhysicalConnectionsPacket(genericPacket)

                if(physicalConnectionsPacket.isEndpoint)
                    endpointConnections[parseHostName(sourceAddress)] = physicalConnectionsPacket.connections[0]
                else {
                    println("Adding physical connections from router: ")
                    physicalConnections[parseHostName(sourceAddress)] = physicalConnectionsPacket.connections

                    for ((routerName, connections) in physicalConnections) {
                        print("Router: $routerName Connections: {")
                        for (connection in connections) {
                            print("$connection,")
                        }
                        println("}")
                    }
                }
            }
            PacketType.TABLE_UPDATE_REQUEST -> sendFlowTable(parseHostName(sourceAddress))
            else -> System.err.println("Controller received wrong packet type!")
        }
    }

    fun sendFlowTables() {
        flowTables = getFlowTables()
        flowTables.keys.forEach { router -> sendFlowTable(router)}
    }

    private fun sendFlowTable(router: String){
        if(flowTables.contains(router)) {
            val table: HashMap<String, String> = flowTables[router]!!
            val packet = FlowTablePacket(table)
            sendPacket(packet, router)
        }
    }

    /**
     * Calculates and returns the flow tables from the end to end routes
     */
    private fun getFlowTables() : HashMap<String, HashMap<String,String>> {
        val routes = getEndToEndRoutes()
        // Key = router hostName; Value = Dest. endpoint, next hop address
        val tables = HashMap<String, HashMap<String,String>>()
        for (route in routes) {
            println("going through each route...")
            // go through the route and insert the relevant hops into the flow tables for each router
            val iterator = route.route.iterator()
            var currentRouter = iterator.next()

            var endFlag = false // used to signal reaching the endpoint in the route

            while(!endFlag){
                var nextRouter: String
                if(iterator.hasNext()) nextRouter = iterator.next()
                else{
                    nextRouter = route.endPoint
                    endFlag = true
                }

                if(tables[currentRouter] == null) tables[currentRouter] = HashMap()
                tables[currentRouter]!![route.endPoint] = nextRouter
                println("Added [$currentRouter] [" + route.endPoint + "] = $nextRouter")

                currentRouter = nextRouter
            }
        }
        return tables
    }

    /**
     * Calculates shortest paths for each combination of endpoints
     */
    private fun getEndToEndRoutes() : List<EndToEndRoute> {
        val listOfRoutes = ArrayList<EndToEndRoute>()

        val endPointsArray = endpointConnections.keys.toTypedArray()
        for (i in endPointsArray.indices) {

            val startPoint = endPointsArray[i]
            val startPointRouter = endpointConnections[startPoint]

            for(j in i+1 until endPointsArray.size){
                val endPoint = endPointsArray[j]
                val endPointRouter = endpointConnections[endPoint]
                if(startPointRouter == null || endPointRouter == null){
                    System.err.println("Start or endpoint is null!")
                    continue
                }

                val shortestPath = findShortestPath(startPointRouter,endPointRouter)
                listOfRoutes.add(EndToEndRoute(startPoint,endPoint,shortestPath))

                // we also want to add the reverse path, to avoid running the algorithm again
                listOfRoutes.add(EndToEndRoute(endPoint,startPoint,shortestPath.reversed()))

                println("Added router to list of routes")
            }
            println("Running from startpoint $startPoint with entry at $startPointRouter")
        }
        return listOfRoutes
    }

    /**
     * Finds the shortest path in the network from endpoint to endpoint
     */
    private fun findShortestPath(startPoint: String, endPoint: String) : List<String> {
        // because we take each hop as a cost of one, we only need to run a breadth-first search to find path

        val outputRoute = LinkedList<String>()
        val queue = ArrayDeque<String>()
        val visited = HashSet<String>()
        queue.add(startPoint) // add entry router
        var currentNode: String

        for ((routerName, connections) in physicalConnections) {
            print("Router: $routerName Connections: {")
            for (connection in connections) {
                print("$connection,")
            }
            println("}")
        }

        while(queue.isNotEmpty()){
            currentNode = queue.remove()
            println("Visited node $currentNode")
            outputRoute.add(currentNode) // append to output

            if(currentNode == endPoint)
                return outputRoute
            else {
                visited.add(currentNode)

                // add current node's neighbours
                physicalConnections[currentNode]?.let { queue.addAll(it) }

                // remove any nodes already visited
                queue.removeAll(visited)
            }
        }
        return emptyList()
    }
}