package openflow

import openflow.packets.EndToEndPacket
import openflow.packets.GenericPacket
import openflow.packets.PacketType
import openflow.packets.PhysicalConnectionsPacket
import java.net.InetAddress

class EndPoint : PhysicallyConnectable, Node() {

    override val connections: HashSet<String> = HashSet()

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val endpoint = EndPoint()
            endpoint.getConnectionsFromUser(1) // only one entry-point to the network
            endpoint.sendPhysicalConnections() // send connection to controller
            endpoint.listen() // listen for incoming messages from other endpoints

            while(true){
                println("Please enter message to send:")
                val message = readLine() ?: continue
                println("Please enter destination host name:")
                val hostName = readLine()?.split("\\s")?.get(0) ?: continue
                val packet = EndToEndPacket(message, hostName)
                // send packet to router connection, for it to be forwarded
                endpoint.sendPacket(packet,endpoint.connections.first())
            }
        }
    }

    override fun onReceipt(genericPacket: GenericPacket, sourceAddress: InetAddress) {
        when(genericPacket.type){
            PacketType.END_TO_END -> {
                val endToEndPacket = EndToEndPacket(genericPacket)
                println(endToEndPacket.message) // print received message
            }
            else -> System.err.println("EndPoint received wrong packet type!")
        }
    }

    /**
     * Sends PHYSICAL_CONNECTIONS packet to given destination
     * Usually to send connections for Controller to process
     */
    fun sendPhysicalConnections(destination: String = "controller") =
        sendPacket(PhysicalConnectionsPacket(true, connections), destination)
}