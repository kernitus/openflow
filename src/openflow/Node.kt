package openflow

import openflow.packets.AcknowledgementPacket
import openflow.packets.GenericPacket
import openflow.packets.PacketType
import java.io.IOException
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress
import java.util.*
import kotlin.collections.HashMap

const val MAX_PACKET_SIZE: Int = 500
const val TIMEOUT: Long = 5000 //ms
const val ACK_TIMEOUT: Long = 500 //ms

/**
 * A node can send and receive packets
 */
abstract class Node (private val sourceSocket: DatagramSocket = DatagramSocket(50000),
                     private val receiverSocket: DatagramSocket = DatagramSocket(50001)) {

    /**
     * Map to store incremental IDs for frames being received
     */
    private val receivedFrameIDs: HashMap<String, Byte> = HashMap()

    /**
     * Stores the last Frame ID for which we sent an each for each given host
     */
    private val lastACKSent: HashMap<String, Byte> = HashMap()

    /**
     * A map that holds a queue of packets to be sent for each destination
     */
    private val packetQueue: HashMap<String, LinkedList<GenericPacket>> = HashMap()

    /**
     * List of packets this node needs to receive acknowledgement for
     * String is host name and byte the frame id
     */
    private val lastAcknowledged: HashMap<String, Byte> = HashMap()

    /**
     * Thread used to listen for incoming packets in the background
     */
    private val listeningThread = Thread(Runnable {
        while(true) {
            val packet = receivePacket()
            if (packet != null) {
                val fromAddress = packet.address
                val hostName = parseHostName(fromAddress)
                val genericPacket = GenericPacket.reconstruct(packet)
                val frameId = genericPacket.frameId

                println("Received " + genericPacket.type + " from " + hostName + " with id " + frameId)

                if (genericPacket.type == PacketType.ACKNOWLEDGEMENT) {
                    acceptAcknowledgement(hostName, frameId)
                } else if (!frameAlreadyReceived(hostName, frameId)) {
                    receivedFrameIDs[hostName] = frameId
                    scheduleCumulativeAcknowledgementTask(hostName)
                    onReceipt(genericPacket, fromAddress)
                }
            }
        }
    })

    /**
     * Tries to receive a packet
     * @return The packet, or null if none was received
     */
    private fun receivePacket(): DatagramPacket? {
        val buffer = ByteArray(MAX_PACKET_SIZE)
        val packet = DatagramPacket(buffer, MAX_PACKET_SIZE)

        try {
            receiverSocket.receive(packet)
            return packet
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return null
    }

    /**
     * Starts a loop to keep receiving packets in the background
     */
    fun listen() = listeningThread.start()

    /**
     * Forcibly stops listening for incoming packets
     * Don't call this if you don't want to potentially lose some packets
     */
    @Suppress("DEPRECATION")
    fun stopListening() = listeningThread.stop()

    /**
     * Called when a packet is received
     */
    abstract fun onReceipt(genericPacket: GenericPacket, sourceAddress: InetAddress)

    /**
     * Gets whether specified frame from address has been already received or not
     */
    private fun frameAlreadyReceived(sourceHostName: String, frameID: Byte) : Boolean {
        val lastReceivedFrameID: Byte? = receivedFrameIDs[sourceHostName]

        return if(lastReceivedFrameID != null && lastReceivedFrameID >= frameID) true
        else {
            receivedFrameIDs[sourceHostName] = frameID
            false
        }
    }

    /**
     * Queues a packet to be sent to a specific destination
     */
    fun queuePacket(packet: GenericPacket, destinationHost: String) {
        if(!packetQueue.containsKey(destinationHost)) packetQueue[destinationHost] = LinkedList()

        val packetList = packetQueue[destinationHost]
        packetList?.add(packet) ?: println("Packet list is null!")
    }

    /**
     * Sends all the packets queued up to given host
     */
    fun sendPackets(destinationHost: String, destinationPort: Int = 50001){
        val packetList = packetQueue[destinationHost]
        if(packetList != null) {
            scheduleAcknowledgementReceiverTask(destinationHost, (packetList.size - 1).toByte())
            // send packets from next after last acknowledged frame onwards
            for (frameId in (lastAcknowledged[destinationHost]?.toInt()?.plus(1) ?: 0) until packetList.size)
                packetList[frameId].send(destinationHost, destinationPort, sourceSocket, frameId.toByte())
        }
    }

    /**
     * Queues this packet and sends all queued packets to given destination
     */
    fun sendPacket(packet: GenericPacket, destinationHost: String){
        queuePacket(packet,destinationHost)
        sendPackets(destinationHost)
    }

    private fun scheduleAcknowledgementReceiverTask(hostName: String, frameID: Byte){
        Timer().schedule(ACKReceiverTask(hostName,frameID), TIMEOUT)
    }

    /**
     * Resends entire window of packet to specified host if we didn't get ACKs for all of them
     */
    inner class ACKReceiverTask(private val hostName: String, private val frameID: Byte) : TimerTask() {
        override fun run() {
            val lastAcknowledgedFrame = lastAcknowledged[hostName]
            if(lastAcknowledgedFrame == null && packetQueue[hostName]?.isNotEmpty() == true ||
                    lastAcknowledgedFrame != null && lastAcknowledgedFrame < frameID)
                sendPackets(hostName,50001)
        }
    }

    private fun scheduleCumulativeAcknowledgementTask(hostName: String){
        Timer().schedule(CumulativeACKTask(hostName), ACK_TIMEOUT)
    }

    /**
     * Sends a cumulative ACK for all unique, ordered frames received from given host
     */
    inner class CumulativeACKTask(private val hostName: String) : TimerTask() {
        override fun run() {
            if(receivedFrameIDs.containsKey(hostName)){
                if(!lastACKSent.containsKey(hostName) || receivedFrameIDs[hostName]!! > lastACKSent[hostName]!!){
                    val lastReceivedFrameID = receivedFrameIDs[hostName]!!
                    sendAcknowledgement(hostName,50001, lastReceivedFrameID)
                    lastACKSent[hostName] = lastReceivedFrameID
                }
            }
        }
    }

    private fun acceptAcknowledgement(hostName: String, frameID: Byte){
        if(lastAcknowledged[hostName]?.compareTo(frameID) ?: -1 < 0)
            lastAcknowledged[hostName] = frameID
    }

    private fun sendAcknowledgement(destinationHost: String, destinationPort: Int, frameID: Byte){
        AcknowledgementPacket().send(destinationHost,destinationPort,sourceSocket,frameID)
    }

    protected fun parseHostName(address: InetAddress) = address.hostName.split(".")[0]
}