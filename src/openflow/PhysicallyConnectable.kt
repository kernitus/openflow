package openflow

interface PhysicallyConnectable {

    /**
     * Hostnames of devices physically connected to this one
     */
    val connections : HashSet<String>

    fun addConnection(hostAddress: String) = connections.add(hostAddress)
    fun removeConnection(hostAddress: String) = connections.remove(hostAddress)

    /**
     * Get list of physical connections from user
     */
    fun getConnectionsFromUser(maxConnections: Int = 50) {
        for(m in 1..maxConnections){
            println("Please enter the hostname of a physical connection, or 'done' to stop:")
            val input: String = readLine()?.split("\\s")?.get(0) ?: "done"
            if(input == "done") break
            connections.add(input)
        }
    }
}