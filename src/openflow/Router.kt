package openflow

import openflow.packets.*
import java.net.InetAddress

class Router : PhysicallyConnectable, Node() {

    override val connections : HashSet<String> = HashSet()

    /**
     * FlowTable with Keys as destination endpoints and
     * Values as router to forward to
     */
    private val flowTable : HashMap<String,String> = HashMap()

    /**
     * Store any packets we didn't know what to do with
     */
    private val unforwardedPackets = ArrayList<EndToEndPacket>()

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val router = Router()
            router.getConnectionsFromUser() // initialise physical connections
            router.sendPhysicalConnections() // send connections to controller
            router.listen()
        }
    }

    override fun onReceipt(genericPacket: GenericPacket, sourceAddress: InetAddress) {
        when(genericPacket.type){
            PacketType.END_TO_END -> forwardPacket(EndToEndPacket(genericPacket))
            PacketType.FLOW_TABLE -> {
                updateFlowTable(FlowTablePacket(genericPacket))
                if(unforwardedPackets.isNotEmpty()){
                    unforwardedPackets.forEach { packet ->
                        run {
                            unforwardedPackets.remove(packet)
                            forwardPacket(packet)
                        }
                    }
                }
            }
            else -> println("Router received wrong packet type " + genericPacket.type)
        }
    }

    private fun forwardPacket(endToEndPacket: EndToEndPacket){
        val nextHopRouterAddress = flowTable[endToEndPacket.destination]
        println("Forwarding packet to " + endToEndPacket.destination)
        if(nextHopRouterAddress == null){
            println("Next hop is null! Contacting Controller...")
            // send the controller a request to update flowtables
            sendPacket(TableUpdateRequestPacket(),"controller")
        }
        nextHopRouterAddress?.let {
            println("Next hop: $nextHopRouterAddress")
            sendPacket(endToEndPacket, nextHopRouterAddress)
        }
    }

    private fun updateFlowTable(flowTablePacket: FlowTablePacket){
        flowTable.clear() // reset the flowtable
        val packetTable = flowTablePacket.table

        if(packetTable.size == 2) { // only if the two columns are there
            val destEndpoints = packetTable[0]
            val nextHops = packetTable[1]

            // loop through both columns, but only until shortest ends
            for (i in 0 until destEndpoints.size.coerceAtMost(nextHops.size)) {
                val destEndPoint = destEndpoints[i]
                val nextHop = nextHops[i]
                flowTable[destEndPoint] = nextHop
            }

        } else System.err.println("Received flowtable does not have exactly two columns!")
    }

    /**
     * Sends PHYSICAL_CONNECTIONS packet to given destination
     * Usually to send connections for Controller to process
     */
    fun sendPhysicalConnections(destination: String = "controller"){
        val packet = PhysicalConnectionsPacket(false, connections)
        sendPacket(packet, destination)
    }
}