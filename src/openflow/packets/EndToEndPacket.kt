package openflow.packets

class EndToEndPacket(val message: String, val destination: String) :
        GenericPacket(PacketType.END_TO_END, arrayOf(message, destination)) {

    constructor(genericPacket: GenericPacket) :
        this(deserialiseArray(genericPacket.extras))

    private constructor(extras: Array<String>) :
            this(extras[0], extras[1])
}