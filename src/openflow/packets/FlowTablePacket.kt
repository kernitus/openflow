package openflow.packets

/**
 * @param table Array of columns for the flowtable, i.e. dest address and next hop address columns
 */
class FlowTablePacket(val table : Array<Array<String>>) :
        GenericPacket(PacketType.FLOW_TABLE, serialiseTable(table)) {

    constructor(genericPacket: GenericPacket):
            this(deserialiseTable(genericPacket.extras))

    constructor(table: HashMap<String,String>) :
            this(arrayOf(table.keys.toTypedArray(), table.values.toTypedArray()))
}