package openflow.packets

import java.io.IOException
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetSocketAddress
import java.net.SocketAddress
import java.nio.ByteBuffer
import kotlin.properties.Delegates

class AcknowledgementPacket : GenericPacket(PacketType.ACKNOWLEDGEMENT)
class TableUpdateRequestPacket : GenericPacket(PacketType.TABLE_UPDATE_REQUEST)

open class GenericPacket(val type: PacketType, val extras: Array<Byte> = emptyArray()) {

    /**
     * Constructor to also send an array of strings in the extras
     */
    constructor(type: PacketType, extraStrings: Array<String>):
            this(type, serialiseArray(extraStrings))

    /**
     * Constructor to also send a single string in the extras
     */
    constructor(type: PacketType, extraStrings: String):
            this(type, arrayOf(extraStrings))

    var frameId by Delegates.notNull<Byte>()
    private var packetSize: Int = 0

    // Generic packet layout:
    // PacketType - Frame ID - extras length? - extras?

    /**
     * Attempts to send the packet to the specified destination
     */
    fun send(destinationHost: String, destinationPort: Int, sourceSocket: DatagramSocket, frameId: Byte) =
            send(InetSocketAddress(destinationHost, destinationPort), sourceSocket, frameId)

    /**
     * Send
     */
    private fun send(socketAddress: SocketAddress, sourceSocket: DatagramSocket, frameId: Byte){
        val packet = DatagramPacket(craftPacket(frameId), packetSize, socketAddress)

        println("Sending $type packet to $socketAddress id $frameId")

        try {
            sourceSocket.send(packet)
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    /**
     * Creates the packet as a ByteArray from given components
     */
    private fun craftPacket(frameId: Byte): ByteArray {
        // Create new byte buffer of variable size
        packetSize = 2 // type + frame ID
        if(extras.isNotEmpty()) packetSize += 1 + extras.size // if any extras, extras size + extras

        val byteBuffer = ByteBuffer.allocate(packetSize)

        byteBuffer.put(type.id) // packet type
        this.frameId = frameId
        byteBuffer.put(frameId) // frame ID

        if(extras.isNotEmpty()) { // insert extras, if any
            byteBuffer.put(extras.size.toByte())
            extras.forEach { byte -> byteBuffer.put(byte) }
        }

        return byteBuffer.array()
    }

    companion object {
        /**
         * Takes a received datagram packet and returns a readable and sendable Packet of correct type
         */
        fun reconstruct(datagramPacket: DatagramPacket): GenericPacket {
            // PacketType - frame ID - extras length? - extras?
            val buffer = ByteBuffer.wrap(datagramPacket.data)

            val type = PacketType.fromId(buffer.get()) // Packet Type
            val frameId = buffer.get() // frame ID

            if(buffer.remaining() != 0){ // There are some extras
                val extrasLength = buffer.get().toInt()
                val currentIndex: Int = buffer.position()

                val packet = GenericPacket(type, buffer.array().copyOfRange(currentIndex, currentIndex + extrasLength).toTypedArray())
                packet.frameId = frameId
                return packet
            }

            return GenericPacket(type)
        }

        /**
         * Serialises arrays of arrays of string (i.e. tables) into a byte array
         */
        fun serialiseTable(table: Array<Array<String>>) : Array<Byte> {
            // for each array of strings in the array of strings,
            // call serialiseArray() and append to output Array<Byte>
            var output = emptyArray<Byte>()
            for(stringArray in table){
                val serialisedArray = serialiseArray(stringArray) // add each serialised string to output
                output += serialisedArray.size.toByte() // store total length of array
                output += serialisedArray
            }
            return output
        }

        fun deserialiseTable(bytes: Array<Byte>) : Array<Array<String>> {
            var output : Array<Array<String>> = emptyArray()
            // for each array of strings in the output, call deserialiseArray and add array to output
            var i  = 0
            while(i < bytes.size){
                val size = bytes[i++].toInt() // length of column
                // copy out exact amount of bytes corresponding to entire column, and deserialise
                output += deserialiseArray(bytes.copyOfRange(i,i + size)) // deserialise string length + contents
                i += size // advance in the byte array to next column
            }
            return output
        }

        /**
         * Serialises an array of strings into an array of bytes that can be sent as an extra
         */
        fun serialiseArray(array: Array<out String>): Array<Byte> {
            var output = emptyArray<Byte>()
            for(string in array){
                output += string.length.toByte() // add string length
                for(c in string.toCharArray()) // add each character of the string
                    output += c.toByte()
            }
            return output
        }

        /**
         * Deserialises an array of bytes into an array of strings
         */
        fun deserialiseArray(arr: Array<Byte>) : Array<String> {
            var output = emptyArray<String>()

            var byteIndex = 0
            while(byteIndex < arr.size) {
                val stringSize = arr[byteIndex++].toInt() // get size of this string
                var string = "" // string to put into output arr

                for (i in 0 until stringSize) { // get all of the string's characters
                    val c: Char = arr[byteIndex++].toChar()
                    string += c
                }

                output += string // put string into output array
            }
            return output
        }
    }
}
