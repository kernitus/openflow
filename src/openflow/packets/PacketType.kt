package openflow.packets

enum class PacketType(val id: Byte) {

    ACKNOWLEDGEMENT(0), PHYSICAL_CONNECTIONS(1), FLOW_TABLE(2), END_TO_END(3), TABLE_UPDATE_REQUEST(4);

    companion object {
        fun fromId(id: Byte): PacketType {
            for (value in values())
                if (value.id == id)
                    return value

            return ACKNOWLEDGEMENT
        }
    }
}