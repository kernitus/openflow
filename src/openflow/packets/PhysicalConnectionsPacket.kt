package openflow.packets

class PhysicalConnectionsPacket(val isEndpoint: Boolean, val connections: Array<String>) :
        GenericPacket(PacketType.PHYSICAL_CONNECTIONS,
                mergeBytes(if(isEndpoint) 1 else 0, serialiseArray(connections))) {

    constructor(genericPacket: GenericPacket):
            this(genericPacket.extras)

    constructor(bytes: Array<Byte>):
            this(bytes[0] != 0.toByte(),
                    deserialiseArray(bytes.copyOfRange(1,bytes.size)))

    constructor(isEndpoint: Boolean, connections: Set<String>):
            this(isEndpoint, connections.toTypedArray())
    
    companion object {

        fun mergeBytes(byte: Byte, arr: Array<Byte>) : Array<Byte>{
            var ba = arrayOf(byte)
            ba += arr
            return ba
        }
    }
}
